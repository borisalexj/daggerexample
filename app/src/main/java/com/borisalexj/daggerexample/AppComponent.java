package com.borisalexj.daggerexample;

import dagger.Component;

@Component(modules = {StorageModule.class, NetworkModule.class})
public interface AppComponent {
//    NetworkUtils getNetworkUtils();
//    DatabaseHelper getDatabaseHelper();
    void injectsMainActivity(MainActivity mainActivity);
}